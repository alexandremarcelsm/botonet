#### LOAD PACKAGES ####

if(!require(igraph)){install.packages('igraph'); library(igraph)}
if(!require(tidyverse)){install.packages('tidyverse'); library(tidyverse)}
if(!require(dplyr)){install.packages('dplyr'); library(dplyr)}
if(!require(vegan)){install.packages('vegan'); library(vegan)}
if(!require(ggplot2)){install.packages('ggplot2'); library(ggplot2)}
if(!require(asnipe)){install.packages('asnipe'); library(asnipe)}
if(!require(magrittr)){install.packages('magrittr'); library(magrittr)}
if(!require(stringr)){install.packages('stringr'); library(stringr)}
if(!require(corrplot)){install.packages('corrplot'); library(corrplot)}
if(!require(assortnet)){install.packages('assortnet'); library(assortnet)}
if(!require(devtools)){install_github("devtools"); library(devtools)}
if(!require(patchwork)){install_github("thomasp85/patchwork"); library(patchwork)}


# load functions ====
source("02_functions.R")
source("https://gist.githubusercontent.com/benmarwick/2a1bb0133ff568cbe28d/raw/fb53bd97121f7f9ce947837ef1a4c65a73bffb3f/geom_flat_violin.R")

# load data ====
load(file = "./03_AMSM_homophily_data.RData")

# set number of permutations
Npermute <- 1000

#### Modularity HWI Real ----
# Modularity GAI real ----
modularity_HWI_real <- vector()
modularity_size_HWI_real <- list()

for(i in 1:length(list_HWI_real)){
  tmp1 <- graph.adjacency(list_HWI_real[[i]], 
                          mode="undirected",
                          weighted=TRUE,
                          diag=FALSE)
  tmpmod <- cluster_leading_eigen(graph = tmp1,
                                  weights = E(tmp1)$weight)
  
  modularity_HWI_real[[i]] <- modularity(tmpmod)
  modularity_size_HWI_real[[i]] <- table(tmpmod$membership)
}

#### Modularity HWI Random ----
modularity_HWI_random <- NULL
listMOD_HWI_random <- list()
for(i in 1:length(list_HWI_real)){
  for(j in 1:Npermute){
    tmp.graph <- graph.adjacency(as.matrix(list_RANDOM_HWI[[i]][[j]]),
                                 mode = "undirected",
                                 diag = FALSE,
                                 weighted = TRUE)
    #tmp.graph <- giant.component(tmp.graph)
    tmpmod2 <- cluster_leading_eigen(graph = tmp.graph,
                                     weights = E(tmp.graph)$weight,
                                     options=list(maxiter=1000000))
    
    modularity_HWI_random[j] <- modularity(tmpmod2)
  }
  listMOD_HWI_random[[i]] <- modularity_HWI_random
}

#### SD random HWI ----
SDhwi_random <- list()
for(i in 1:length(list_RANDOM_HWI)){
  sdtmp <- vector()
  for(k in 1:Npermute){
    sdtmp[k] <- sd(matrix_unfold(list_RANDOM_HWI[[i]][[k]]))
  }
  SDhwi_random[[i]] <- sdtmp
}

#### Descriptive results ----

# FP ----
mean(info_IDrestrict$Freq_Int, na.rm = T)
sd(info_IDrestrict$Freq_Int, na.rm = T)

# HRO ----
mean(info_IDrestrict$HR, na.rm = T)
sd(info_IDrestrict$HR, na.rm = T)

mean(matrix_unfold(traits_by_age$hro))
sd(matrix_unfold(traits_by_age$hro))

# Sex ----
info_IDrestrict$SEX %>% 
  na.omit() %>% 
  table()


# Genetic Relatedness ----
mean(matrix_unfold(subr.matr))
sd(matrix_unfold(subr.matr))

#### MRQAP ####

# MRQAP subset 12 individuals ----
mrqap.related <- list()
for(i in 1:4){
  mrqap.related[[i]] <- mrqap.dsp(list_HWI_related[[i]] ~
                                    traits_by_related$coop +
                                    traits_by_related$hro + 
                                    traits_by_related$age +
                                    traits_by_related$sex +
                                    traits_by_related$related,
                                  randomisations = 2000, 
                                  intercept = FALSE,
                                  test.statistic = "beta")
}

# Mantel HWI ~ R ----
ade4::mantel.rtest(as.dist(list_HWI_related[[1]]), as.dist(traits_by_related$related), 9999)

# MRQAP subset 30 individuals ----
mrqap.sex <- list()
for(i in 1:4){
  mrqap.sex[[i]] <- mrqap.dsp(list_HWI_sex[[i]] ~
                                traits_by_sex$coop +
                                traits_by_sex$hro + 
                                traits_by_sex$age +
                                traits_by_sex$sex,
                              randomisations = 2000, 
                              intercept = FALSE,
                              test.statistic = "beta")
}

# MRQAP 34 individuals ----
mrqap.age <- list()
for(i in 1:4){
  mrqap.age[[i]] <- mrqap.dsp(list_HWI_real[[i]] ~ 
                                traits_by_age$hro + 
                                traits_by_age$coop + 
                                traits_by_age$age,
                              randomisations = 2000, intercept = FALSE,
                              test.statistic = "beta")
}

print.mrqap.dsp(mrqap.age[[1]])
print.mrqap.dsp(mrqap.age[[2]])
print.mrqap.dsp(mrqap.age[[3]])
print.mrqap.dsp(mrqap.age[[4]])

#### GLM data ####
GLMdata <- data.frame(HROunfold = matrix_unfold(traits_by_age$hro),
                      AGEunfold = matrix_unfold(traits_by_age$age),
                      COOPunfold = matrix_unfold(traits_by_age$coop))

#### GAI Real ####
listGAI_real <- list()
SDgai_real <- list()

for(i in 1:length(list_HWI_real)){
  resids <- vector()
  sd.gai <- vector()
  sd.list <- list()
  residsLIST <- list()
  tmp <- matrix_unfold(list_HWI_real[[i]])
  tmp.denmat <- matrix_unfold(list_DENMAT_real[[i]])
  tmp.glm <- glm(cbind(tmp, (1 - tmp)) ~ HROunfold, 
                 data = GLMdata,
                 family = "binomial")
  resids <- unname(residuals.glm(tmp.glm, type = "deviance"))
  listGAI_real[[i]] <- resids/tmp.denmat
  SDgai_real[[i]] <- sd(resids/tmp.denmat)
}

# Cooperative context
tmp.coop <- matrix_unfold(list_HWI_real[[2]])
tmp.coop.denmat <- matrix_unfold(list_DENMAT_real[[2]])
tmp.coop.glm <- glm(cbind(tmp.coop, (1 - tmp.coop)) ~ HROunfold,
                    data=GLMdata,
                    family = "binomial")
resids.coop <- unname(residuals.glm(tmp.coop.glm, 
                                    type = "deviance"))

listGAI_real[[2]] <- resids.coop/tmp.coop.denmat
SDgai_real[[2]] <- sd(resids.coop/tmp.coop.denmat)

str(listGAI_real) # GAI unfolded
str(SDgai_real) # GAI SD real

# Modularity GAI real ----
modularity_GAI_real <- vector()
modularity_size_GAI_real <- list()
listMATRIX_GAI_real <- list()

for(i in 1:length(listGAI_real)){
  tmpmat <- matrix_folding(matrix.unfold = listGAI_real[[i]],
                           matrix.labels = rownames(list_HWI_real[[i]]),
                           matrix.reference = list_HWI_real[[1]])
  
  tmp1 <- graph.adjacency(as.matrix(tmpmat), 
                          mode="undirected",
                          weighted=TRUE,
                          diag=FALSE)
  tmpmod3 <- cluster_leading_eigen(graph = tmp1,
                                   weights = E(tmp1)$weight,
                                   options=list(maxiter=1000000))
  
  listMATRIX_GAI_real[[i]] <- tmpmat
  modularity_GAI_real[[i]] <- modularity(tmpmod3)
  modularity_size_GAI_real[[i]] <- table(tmpmod3$membership)
}


#### GAI RANDOM ####

listGAI_random <- list()
SDgai_random <- list()
listMATRIX_GAI_random <- list()

for(i in 1:length(list_RANDOM_HWI)){
  resids <- vector()
  sd.gai <- vector()
  sd.list <- list()
  residsLIST <- list()
  tmp.GAImat <- list()
  
  for(j in 1:Npermute){
    tmp <- matrix_unfold(list_RANDOM_HWI[[i]][[j]])
    tmp.glm <- glm(cbind(tmp, (1 - tmp)) ~ HROunfold + COOPunfold, 
                   data = GLMdata,
                   family = "binomial")
    resids <- unname(residuals.glm(tmp.glm, type = "deviance"))
    residsLIST[[j]] <- resids/(matrix_unfold(list_RANDOM_DENMAT[[i]][[j]]))
    sd.gai[j] <- sd(resids/(matrix_unfold(list_RANDOM_DENMAT[[i]][[j]])))
    tmp.GAImat[[j]] <- matrix_folding(matrix.unfold = residsLIST[[j]],
                                      matrix.labels = IDlabels,
                                      matrix.reference = list_HWI_real[[1]])
  }
  listGAI_random[[i]] <- residsLIST
  SDgai_random[[i]] <- sd.gai
  listMATRIX_GAI_random[[i]] <- tmp.GAImat
}

# Cooperative context
residsLIST.coop <- list()
sd.gai.coop <- vector()
tmp.GAImat.coop <- list()

for(j in 1:Npermute){
  tmp.coop.random <- matrix_unfold(list_RANDOM_HWI[[2]][[j]])
  tmp.coop.glm.random <- glm(cbind(tmp.coop.random, (1 - tmp.coop.random)) ~ HROunfold, 
                             data = GLMdata,
                             family = "binomial")
  resids.coop.random <- unname(residuals.glm(tmp.coop.glm.random, 
                                             type = "deviance"))
  residsLIST.coop[[j]] <- resids.coop.random/(matrix_unfold(list_RANDOM_DENMAT[[2]][[j]]))
  sd.gai.coop[j] <- sd(resids.coop.random/(matrix_unfold(list_RANDOM_DENMAT[[2]][[j]])))
  tmp.GAImat.coop[[j]] <- matrix_folding(matrix.unfold = residsLIST.coop[[j]],
                                         matrix.labels = IDlabels,
                                         matrix.reference = list_HWI_real[[1]])
}
listGAI_random[[2]] <- residsLIST.coop
SDgai_random[[2]] <- sd.gai.coop
listMATRIX_GAI_random[[2]] <- tmp.GAImat.coop

str(listGAI_random) # GAI Random unfolded
str(SDgai_random) # SD GAI Random
str(listMATRIX_GAI_random) # List with GAI lists by context  

#### Modularity GAI Random ####

listMOD_GAI_random <- list()
modularity_GAI_random <- vector()
sizemod_gai_rand_vec <- vector()
sizemod_GAI_random <- list()

for(i in 1:length(listMATRIX_GAI_random)){
  modularity_GAI_random <- vector()
  assort_random <- vector()
  for(j in 1:Npermute){
    
    tmp1 <- graph.adjacency(listMATRIX_GAI_random[[i]][[j]], 
                            mode="undirected",
                            weighted=TRUE,
                            diag=FALSE)
    tmpmod4 <- cluster_leading_eigen(graph = tmp1,
                                     weights = E(tmp1)$weight,
                                     options = list(maxiter = 1000000))
    modularity_GAI_random[j] <- modularity(tmpmod4)
    sizemod_gai_rand_vec[j] <- length(communities(tmpmod4))
  }
  listMOD_GAI_random[[i]] <- modularity_GAI_random
}


### Make GAI matrices symmetrics ####

list_SYM_MATRIX_GAI_real <- listMATRIX_GAI_real

for(i in 1:4){
  list_SYM_MATRIX_GAI_real[[i]][upper.tri(list_SYM_MATRIX_GAI_real[[i]])] = t(list_SYM_MATRIX_GAI_real[[i]])[upper.tri(t(list_SYM_MATRIX_GAI_real[[i]]))]
}

list_SYM_MATRIX_GAI_random <- listMATRIX_GAI_random

for(i in 1:4){
  for(j in 1:Npermute){
    list_SYM_MATRIX_GAI_random[[i]][[j]][upper.tri(list_SYM_MATRIX_GAI_random[[i]][[j]])] = t(list_SYM_MATRIX_GAI_random[[i]][[j]])[upper.tri(t(list_SYM_MATRIX_GAI_random[[i]][[j]]))]
  }
}

#### Assortativity GAI random ####

ord.df <- info_IDrestrict[,c("Id", "Freq_Int", "HR")]
rownames(ord.df) <- info_IDrestrict$Id
ord.df <- ord.df[order(ord.df$Id),]
ord.df$Freq_Int <- ord.df$Freq_Int/100

listASSORT_FP_GAI_random <- list()
listASSORT_HR_GAI_random <- list()

for(i in 1:length(list_SYM_MATRIX_GAI_random)){
  
  assortFP_random <- vector()
  assortHR_random <- vector()
  
  for(j in 1:Npermute){
    
    tmp.fp <- assortment.continuous(list_SYM_MATRIX_GAI_random[[i]][[j]], 
                                    vertex_values = ord.df$Freq_Int,
                                    weighted = TRUE, 
                                    SE = FALSE)
    assortFP_random[j] <- tmp.fp[[1]]
    
    tmp.hr <- assortment.continuous(list_SYM_MATRIX_GAI_random[[i]][[j]],
                                    vertex_values = ord.df$HR,
                                    weighted = TRUE, 
                                    SE = FALSE)
    assortHR_random[j] <- tmp.hr[[1]]
    
  }
  
  listASSORT_FP_GAI_random[[i]] <- assortFP_random
  listASSORT_HR_GAI_random[[i]] <- assortHR_random
  
}

#### Assortativity GAI Obs ####

listASSORT_FP_GAI_real <- list()
listASSORT_HR_GAI_real <- list()

for(i in 1:length(list_SYM_MATRIX_GAI_real)){
  
  listASSORT_FP_GAI_real[[i]] <- assortment.continuous(list_SYM_MATRIX_GAI_real[[i]],
                                                       vertex_values = ord.df$Freq_Int,
                                                       weighted = TRUE,
                                                       SE = FALSE)  
  
  listASSORT_HR_GAI_real[[i]] <- assortment.continuous(list_SYM_MATRIX_GAI_real[[i]],
                                                       vertex_values = ord.df$HR,
                                                       weighted = TRUE,
                                                       SE = FALSE) 
  
}


#### Assortativity HWI Random ####

listASSORT_FP_HWI_random <- list()
listASSORT_HR_HWI_random <- list()

for(i in 1:length(list_RANDOM_HWI)){
  
  hwi_assortFP_random <- vector()
  hwi_assortHR_random <- vector()
  
  for(j in 1:Npermute){
    
    hwi.rand.tmp.fp <- assortment.continuous(list_RANDOM_HWI[[i]][[j]], 
                                             vertex_values = ord.df$Freq_Int,
                                             weighted = TRUE, 
                                             SE = FALSE)
    hwi_assortFP_random[j] <- hwi.rand.tmp.fp[[1]]
    
    hwi.rand.tmp.hr <- assortment.continuous(list_RANDOM_HWI[[i]][[j]],
                                             vertex_values = ord.df$HR,
                                             weighted = TRUE, 
                                             SE = FALSE)
    hwi_assortHR_random[j] <- hwi.rand.tmp.hr[[1]]
    
  }
  
  listASSORT_FP_HWI_random[[i]] <- hwi_assortFP_random
  listASSORT_HR_HWI_random[[i]] <- hwi_assortHR_random
  
}

#### Assortativity HWI Obs ####

listASSORT_FP_HWI_real <- list()
listASSORT_HR_HWI_real <- list()

for(i in 1:length(list_HWI_real)){
  
  listASSORT_FP_HWI_real[[i]] <- assortment.continuous(list_HWI_real[[i]],
                                                       vertex_values = ord.df$Freq_Int,
                                                       weighted = TRUE,
                                                       SE = FALSE)  
  
  listASSORT_HR_HWI_real[[i]] <- assortment.continuous(list_HWI_real[[i]],
                                                       vertex_values = ord.df$HR,
                                                       weighted = TRUE,
                                                       SE = FALSE) 
  
}

#### FIGURES ####

# Social preferences ====

names(SDhwi_random) <- namesCONTEXT
names(SDgai_random) <- namesCONTEXT
names(list_SD_HWI_real) <- namesCONTEXT

dfsd.hwi <- data.frame()
dfsd.gai <- data.frame()

for(i in 1:4){
  
  tmp.ci.sd.hwi <- quantile(SDhwi_random[[i]],
                            probs = c(0.025, 0.975),
                            type = 2)  
  
  tmp.ci.sd.gai <- quantile(SDgai_random[[i]],
                            probs = c(0.025, 0.975),
                            type = 2)
  
  dfsd.hwi[i, "SD"] <- list_SD_HWI_real[[i]]
  dfsd.hwi[i, "context"] <- namesCONTEXT[i]
  dfsd.hwi[i, "lowCI"] <- tmp.ci.sd.hwi[1]
  dfsd.hwi[i, "uprCI"] <- tmp.ci.sd.hwi[2]
  dfsd.hwi[i, "index"] <- "HWI"
  
  dfsd.gai[i, "SD"] <- SDgai_real[[i]]
  dfsd.gai[i, "context"] <- namesCONTEXT[i]
  dfsd.gai[i, "lowCI"] <- tmp.ci.sd.gai[1]
  dfsd.gai[i, "uprCI"] <- tmp.ci.sd.gai[2]
  dfsd.gai[i, "index"] <- "GAI"
  
}

dfSD_real <- rbind(dfsd.hwi, dfsd.gai) %>% 
  mutate(index = factor(index, levels = c("HWI", "GAI")))

dfSD_random <- cbind(
  rbind(reshape2::melt(SDgai_random),
        reshape2::melt(SDhwi_random)),
  rep(c("GAI", "HWI"), each = 4000)) 
names(dfSD_random) <- c("SD", "context", "index")
dfSD_random$index <- factor(dfSD_random$index, 
                            levels = c("HWI", "GAI"))


# Plot standard deviation ====

gplot.sd <- ggplot(dfSD_random,
                   aes(x = context, y = SD,
                       fill = index)) +
  geom_flat_violin(trim = FALSE,
                   scale = "width",
                   adjust = 2,
                   colour = "NA") +
  facet_wrap(context ~ index, ncol = 4, scales = "free", dir = "v") +
  scale_fill_manual(values=c(alpha("grey80", 0.5), 
                             alpha("#D55E00", 0.25))) +
  scale_x_discrete(labels = c("HWI", "GAI")) +
  scale_y_continuous(breaks = equal_breaks(3, 0.05),
                     minor_breaks = FALSE,
                     expand = c(0.05, 0),
                     #label = scientific_10,
                     label = function(x){round(x, digits = 4)}) +
  geom_errorbar(data = dfSD_real, 
                aes(x = context,
                    ymin = lowCI, 
                    ymax = uprCI),
                size = 0.5,
                width = 0.075, 
                color = "#0000ff96") +
  geom_point(data = dfSD_real, 
             aes(x = context, 
                 y = SD),
             size = 2,
             color = "#ff000096") +
  theme_violin + 
  theme(axis.text.y = element_text(angle = 0, hjust = 0.5)) +
  coord_flip() 


# Social division ====
names(modularity_HWI_real) <- namesCONTEXT
names(modularity_GAI_real) <- namesCONTEXT
names(listMOD_HWI_random) <- namesCONTEXT
names(listMOD_GAI_random) <- namesCONTEXT

dfmod.hwi <- data.frame()
dfmod.gai <- data.frame()

for(i in 1:4){
  
  tmp.ci.Q.hwi <- quantile(listMOD_HWI_random[[i]],
                           probs = c(0.025, 0.975),
                           type = 2)  
  
  tmp.ci.Q.gai <- quantile(listMOD_GAI_random[[i]],
                           probs = c(0.025, 0.975),
                           type = 2)
  
  dfmod.hwi[i, "Q"] <- modularity_HWI_real[[i]]
  dfmod.hwi[i, "context"] <- namesCONTEXT[i]
  dfmod.hwi[i, "lowCI"] <- tmp.ci.Q.hwi[1]
  dfmod.hwi[i, "uprCI"] <- tmp.ci.Q.hwi[2]
  dfmod.hwi[i, "index"] <- "HWI"
  
  dfmod.gai[i, "Q"] <- modularity_GAI_real[[i]]
  dfmod.gai[i, "context"] <- namesCONTEXT[i]
  dfmod.gai[i, "lowCI"] <- tmp.ci.Q.gai[1]
  dfmod.gai[i, "uprCI"] <- tmp.ci.Q.gai[2]
  dfmod.gai[i, "index"] <- "GAI"
  
}

dfmod_real <- rbind(dfmod.hwi, dfmod.gai) %>% 
  mutate(index = factor(index, levels = c("HWI", "GAI")))

dfmod_random <- cbind(
  rbind(reshape2::melt(listMOD_HWI_random),
        reshape2::melt(listMOD_GAI_random)),
  rep(c("HWI", "GAI"), each = 4000)) 
names(dfmod_random) <- c("Q", "context", "index")
dfmod_random$index <- factor(dfmod_random$index,
                             levels = c("HWI", "GAI"))

# Plot Modularity ====

gplot.mod <- ggplot(dfmod_random,
                    aes(x = factor(index), 
                        y = Q, 
                        fill = index)) +
  geom_flat_violin(trim = FALSE,
                   scale = "width",
                   adjust = 2,
                   colour = "NA") +
  facet_wrap( ~ context, ncol = 4, scales = "free_y") +
  scale_y_continuous(breaks = equal_breaks(4, 0.05),
                     minor_breaks = FALSE,
                     expand = c(0.05, 0),
                     limits = c(0, 0.6)) +
  scale_fill_manual(values=c(alpha("grey80", 0.5), 
                             alpha("#D55E00", 0.25))) +
  scale_x_discrete(limits = c("GAI", "HWI")) +
  geom_errorbar(data = dfmod_real, 
                aes(x = factor(index),
                    ymin = lowCI, 
                    ymax = uprCI),
                size = 0.5,
                width = 0.075, 
                color = "#0000ff96") +
  geom_point(data = dfmod_real, 
             aes(x = factor(index), 
                 y = Q),
             size = 2,
             color = "#ff000096") +
  theme_violin +
  coord_flip()

# Assortativity Data frame ====

names(listASSORT_FP_GAI_random) <- namesCONTEXT
names(listASSORT_FP_GAI_real) <- namesCONTEXT
names(listASSORT_HR_GAI_random) <- namesCONTEXT
names(listASSORT_HR_GAI_real) <- namesCONTEXT
names(listASSORT_FP_HWI_random) <- namesCONTEXT
names(listASSORT_FP_HWI_real) <- namesCONTEXT
names(listASSORT_HR_HWI_random) <- namesCONTEXT
names(listASSORT_HR_HWI_real) <- namesCONTEXT

dfgai.fp <- data.frame()
dfhwi.fp <- data.frame()
dfgai.hr <- data.frame()
dfhwi.hr <- data.frame()


for(i in 1:4){
  tmp.ci.gaiFP <- quantile(listASSORT_FP_GAI_random[[i]],
                           probs = c(0.025,0.975),
                           type = 2)
  
  tmp.ci.hwiFP <- quantile(listASSORT_FP_HWI_random[[i]],
                           probs = c(0.025, 0.975),
                           type = 2)
  
  tmp.ci.gaiHR <- quantile(listASSORT_HR_GAI_random[[i]],
                           probs = c(0.025,0.975),
                           type = 2)
  
  tmp.ci.hwiHR <- quantile(listASSORT_HR_HWI_random[[i]],
                           probs = c(0.025, 0.975),
                           type = 2)
  
  dfgai.fp[i, "assort"] <- listASSORT_FP_GAI_real[[i]]
  dfgai.fp[i, "context"] <- namesCONTEXT[i]
  dfgai.fp[i, "lowCI"] <- tmp.ci.gaiFP[1]
  dfgai.fp[i, "uprCI"] <- tmp.ci.gaiFP[2]
  dfgai.fp[i, "index"] <- "GAI"
  dfgai.fp[i, "variable"] <- "FP"
  
  dfhwi.fp[i, "assort"] <- listASSORT_FP_HWI_real[[i]]
  dfhwi.fp[i, "context"] <- namesCONTEXT[i]
  dfhwi.fp[i, "lowCI"] <- tmp.ci.hwiFP[1]
  dfhwi.fp[i, "uprCI"] <- tmp.ci.hwiFP[2]
  dfhwi.fp[i, "index"] <- "HWI"
  dfhwi.fp[i, "variable"] <- "FP"
  
  dfgai.hr[i, "assort"] <- listASSORT_HR_GAI_real[[i]]
  dfgai.hr[i, "context"] <- namesCONTEXT[i]
  dfgai.hr[i, "lowCI"] <- tmp.ci.gaiHR[1]
  dfgai.hr[i, "uprCI"] <- tmp.ci.gaiHR[2]
  dfgai.hr[i, "index"] <- "GAI"
  dfgai.hr[i, "variable"] <- "HR"
  
  dfhwi.hr[i, "assort"] <- listASSORT_HR_HWI_real[[i]]
  dfhwi.hr[i, "context"] <- namesCONTEXT[i]
  dfhwi.hr[i, "lowCI"] <- tmp.ci.hwiHR[1]
  dfhwi.hr[i, "uprCI"] <- tmp.ci.hwiHR[2]
  dfhwi.hr[i, "index"] <- "HWI"
  dfhwi.hr[i, "variable"] <- "HR"
  
}

dfASSORT <- rbind(dfgai.fp, dfhwi.fp, dfgai.hr, dfhwi.hr) %>% 
  mutate(index = factor(index, levels = c("HWI", "GAI")))

dfASSORT_random <- cbind(
  rbind(reshape2::melt(listASSORT_FP_GAI_random),
        reshape2::melt(listASSORT_FP_HWI_random),
        reshape2::melt(listASSORT_HR_GAI_random),
        reshape2::melt(listASSORT_HR_HWI_random)),
  rep(c("GAI", "HWI"), each = 4000, times = 2),
  rep(c("FP", "HR"), each = 8000))
names(dfASSORT_random) <- c("assort", "context", "index", "variable")
dfASSORT_random$index <- factor(dfASSORT_random$index, levels = c("HWI", "GAI"))

# Plot assortativity ====

gplot.assort <- ggplot(dfASSORT_random,
                       aes(x = factor(index), 
                           y = assort, 
                           fill = index)) +
  geom_flat_violin(
    trim = FALSE,
    scale = "width",
    adjust = 2,
    colour = "NA") +
  scale_fill_manual(values=c(alpha("grey80", 0.5), 
                             alpha("#D55E00", 0.25))) +
  scale_y_continuous(breaks = equal_breaks(4, 0.05),
                     minor_breaks = FALSE,
                     expand = c(0.05, 0),
                     limits = c(0.25, 1)) +
  scale_x_discrete(limits = c("GAI", "HWI")) +
  facet_wrap(variable ~ context, 
             ncol = 4, scales = "free_y") +
  geom_errorbar(data = dfASSORT, 
                aes(x = factor(index),
                    ymin = lowCI, 
                    ymax = uprCI),
                size = 0.5,
                width = 0.075, 
                color = "#0000ff96") +
  geom_point(data = dfASSORT, 
             aes(x = factor(index), 
                 y = assort),
             size = 2,
             color = "#ff000096") +
  theme_violin +
  coord_flip()


# PLOT FIGURE 01 ====

pdf(file = "./Figures/Figure01_long02_SD_Q_Assort_Color01.pdf",
    height = 6, width = 7)

(gplot.sd / gplot.mod / gplot.assort) + 
  plot_layout(heights = c(0.45, 0.45, 0.9))

dev.off()

#### PLOT NETWORKS ====

# Colorblind palette
color.mod <- c("#E69F00", # orange
               "#56B4E9", # sky blue
               "#009E73", # bluish green
               "#F0E442", # yellow
               "#D55E00", # vermilion
               "#CC79A7", # reddish purple
               "#0072B2", # blue
               "#999999") # grey


svg(file = "./Figures/Fig01_networks_v3.svg",
    height = 9.2, width = 7.2)
# HWI Networks 
par(mfrow = c(5,4), mar = c(0,1,0,1))

layout.net <- list()

for(i in 1:length(list_HWI_real)){
  hwinet <- graph.adjacency(as.matrix(list_HWI_real[[i]]),
                            mode = "undirected",
                            weighted = TRUE,
                            diag = FALSE)
  mod.hwi <- cluster_leading_eigen(hwinet)
  layout.net[[i]] <- igraph::layout.fruchterman.reingold(hwinet)
  plot.igraph(hwinet,
              layout = layout.net[[i]],
              main = namesCONTEXT[i],
              edge.color = alpha('grey80', 0.9),
              vertex.label = NA,
              vertex.size = 12,
              edge.width = E(hwinet)$weight * 10,
              vertex.color = color.mod[mod.hwi$membership])
}

# GAI NETWORKS ----
for(i in 1:length(listMATRIX_GAI_real)){
  gainet <- graph.adjacency(as.matrix(listMATRIX_GAI_real[[i]]),
                            mode = "undirected",
                            weighted = TRUE,
                            diag = FALSE)
  mod.gai <- cluster_leading_eigen(graph = gainet,
                                   weights = E(gainet)$weight,
                                   options = list(maxiter=1000000))
  plot.igraph(gainet,
              layout = layout.net[[i]],
              main = namesCONTEXT[i],
              edge.color = scales::alpha('#D55E00', 0.25),
              vertex.label = NA,
              vertex.size = 12,
              edge.width = 5 * rescale(E(gainet)$weight, r.out = range(E(hwinet)$weight)),
              vertex.color = color.mod[mod.gai$membership])
}

dev.off()
