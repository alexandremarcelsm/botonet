# README #

### **Author, maintainer and contact**

Alexandre M. S. Machado

*Departamento de Ecologia e Zoologia, Universidade Federal de Santa Catarina, Caixa Postal 5102, CEP 88040-970, Florianopolis, SC, Brazil.*

Email: alexandremarcelsm@hotmail.com

### **Description**

This repository includes all the R scripts to reproduce the analyses and plot the figures of the manuscript:
 
Machado, AMS; Cantor, M; Costa, APB; Righetti, BPH; Bezamat, C; Valle-Pereira, JVS; Simões-Lopes, PC; Castilho, PV; Daura-Jorge, FG. 2018. *Homophily around specialized foraging underlies dolphin social preferences*. 
 
More information about the R Session used to run the analyses can be found in supplementary material.

### **Files**

**01_REV01_GAI_SRI_MAT_dev.R**     

Summary: This file contains the R Script to run the analyses and plot the figures of the manuscript. 

**02_functions.R**

Summary: This file contains the R Script to run the custom functions required in the file *01_REV01_GAI_SRI_MAT_dev.R*.

### **Data**

The data is available at the [Dryad repository](), where you can find the following files:

**01_Dryad_DJ12_groups.csv**

This file contains five variables of 503 groups (rows). 

1. `date` contains information of the sampling period;
2. `calf` is the presence or absence of calves in the group;
3. `behavior` contains information of the behavioural state (D = travelling; DP = travelling and foraging; DS = travelling and socializing; P = foraging; S = socializing);
4. `coop` contains information about the cooperative foraging behaviour, where *S* refer to groups engaged in cooperative foraging and *N* to groups that were foraging independently from fishermen.
5. `IDs` contains the identification of all individuals observed in the groups.

**02_Dryad_DJ12_homerange.csv**

This file contains a 35x35 matrix with home range overlap values, ranging from 0 (no overlap) to 1 (completely overlapped). 

**03_Dryad_DJ12_id_cov.csv**

This file contains nine covariables of 52 photo-identified individuals.

1. `Id` contains the identity of all 52 photo-identified individuals;
2. `Freq_Int` contains the relative frequency of participation (*fp*), ranging from 0 to 100%;
3. `SEX` contains the sex of each individual, where M refer to male individuals, F to femeales and I for those individuals that we were unable to determine the sex;
4. `age` contains the age class for each individual, classified as adult or senior (individuals observed in the area for more than 30 years);
5. `HR` is the home range area in km², estimated via fixed kernel methods with a 95% probability contour for each individual

**04_Dryad_DJ12_relatedness_Q.csv**

This file contains the pairwise genetic relatedness (Queller and Goodnight index) among 13 individuals (see Supplementary Material).

### **Instructions and variable description**

The first part of the script is used to subset the dataset according to behavioural contexts and covariables available. As described in Supplementary Material, we used three subsets of our dataset to run Multiple Regression Quadratig Assignment Prodecure (MRQAP). The association matrices for each subset are in lists `list_SRI_related`, `list_SRI_sex` and `list_SRI_real`. The covariables for the respective subsets are in lists `traits_by_related`, `traits_by_sex` and `traits_by_age`. We describe some of the objects in detail below.

`IDlabels`: A character vector containing the ID of 34 individuals;
`namesCONTEXT`: A character vector containing the name of the four behaviour contexts presented in the manuscript ("All_Behaviour"; "Cooperative"; "Non_Cooperative"; "Non_Foraging");    
`info_IDrestrict`: A data frame containing 34 observations (individuals) and five variables:

- `Id`: A character vector containing the ID of 34 individuals;
- `Freq_Int`: Numeric, the relative frequency of participation (*fp*) of each individual;
- `SEX`: Sex of the 34 individuals, including F (female), M (male), I (the sex of the individual could not be determined);
- `age`: Two age classes: “senior” for all individuals observed in the area for more than 30 years based on previous field data, and “adult” for the remaining.
- `HR`: Home range size in km² estimated via fixed kernel methods with a 95% probability contour;

`related.matrix`: A 13x13 matrix containing the genetic pairwise relatedness value for 13 genotyped individuals;     


#### **Subset 1**

`list_SRI_related`: A list containing one 12x12 association matrix for each behavioural context, based on the subset of 12 individuals which all structural variables were known;

`traits_by_related`: A list containing individual traits based on the subset of 12 individuals:

- `coop`: The relative frequency of participation pairwise relationship is given by a 12x12 distance matrix, where *dij*=d(I, J), that is the Euclidean length between the *fpi* and *fpj* ranging from d(I, J)=0 when *fpi*=*fpj* to d(I, J)=+∞. 
- `hro`: A 12x12 matrix in which *hij* is the home range overlap calculated as *HROij* = (*Rij/Ri*) x (*Rij/Rj*), where *Ri* and *Rj* are the total home range size for dolphins *i* and *j*, respectively, and *Rij* is the overlap between their areas;
- `age`: A 12x12 binary matrix in which the age pairwise relationships is given by *aij*=1 when individuals *i* and *j* were of the same age class and *aij*=0 otherwise; 
- *sex:* A 12x12 binary matrix in which the sex pairwise relationships is given by *sij*=1 when individuals *i* and *j* were of the same sex and *sij*=0 otherwise
- `related`: A 12x12 matrix in which *gij* is the mean pairwise relatedness value for genotyped individuals. The genetic relatedness between individuals *i* and *j* was given by the Queller and Goodnight index.

#### **Subset 2**

`list_SRI_sex`: A list containing one 30x30 association matrix for each behavioural context, based on the subset of 30 individuals containing all individual traits but genetic relatedness;     

`traits_by_sex`: A list containing individual traits based on the subset of 30 individuals: 

- `coop`: The relative frequency of participation pairwise relationship is given by a 30x30 distance matrix where *dij*=d(I, J), that is the Euclidean length between the *fpi* and *fpj* ranging from d(I, J)=0 when *fpi*=*fpj* to d(I, J)=+∞. 
- `hro`: A 30x30 matrix in which *hij* is the home range overlap calculated as *HROij* = (*Rij/Ri*) x (*Rij/Rj*), where *Ri* and *Rj* are the total home range size for dolphins *i* and *j*, respectively, and *Rij* is the overlap between their areas;
- `age`: A 30x30 binary matrix in which the age pairwise relationships is given by *aij*=1 when individuals *i* and *j* were of the same age class and *aij*=0 otherwise; 
- `sex`: A 30x30 binary matrix in which the sex pairwise relationships is given by *sij*=1 when individuals *i* and *j* were of the same sex and *sij*=0 otherwise

#### **Subset 3**

`list_SRI_real`: A list containing one 34x34 association matrix for each behavioural context, including all individuals to which information on the frequency of participation, home range overlap and age were available;    

`traits_by_age`: A list containing individual traits based on the subset of 34 individuals: 

- `coop`: The relative frequency of participation pairwise relationship is given by a 34x34 distance matrix where *dij*=d(I, J), that is the Euclidean length between the *fpi* and *fpj* ranging from d(I, J)=0 when *fpi*=*fpj* to d(I, J)=+∞. 
- `hro`: A 34x34 matrix in which *hij* is the home range overlap calculated as *HROij* = (*Rij/Ri*) x (*Rij/Rj*), where *Ri* and *Rj* are the total home range size for dolphins *i* and *j*, respectively, and *Rij* is the overlap between their areas;
- `age`: A 34x34 binary matrix in which the age pairwise relationships is given by *aij*=1 when individuals *i* and *j* were of the same age class and *aij*=0 otherwise; 


#### **Association index (SRI) and Generalized Affiliation Index (GAI)**

`list_SRI_real`: A list containing one 34x34 association matrix for each behavioural context, including all individuals to which information on the frequency of participation, home range overlap and age were available;    

`list_SD_SRI_real`: A list of 4 levels, one for each behaviour context. Each level contains the standard deviation observed for each association matrix;    
`list_RANDOM_SRI`: A list of 4 levels, one for each behaviour context. Each level contains 1000 random association matrices (34x34);    

`list_INMAT_real`: A list of 4 levels, one for each behaviour context. Each level contains one 34x34 matrix, containing the Simple-Ratio Index numerator to calculate the generalized affiliation indices (cf. Whitehead and James, 2015; https://doi.org/10.1111/2041-210X.12383).     
`list_RANDOM_INMAT`: A list of 4 levels, one for each behaviour context. Each level contains 20000 random matrices (34x34), containing the Simple-Ratio Index numerator generated by random matrices to calculate random generalized affiliation indices (cf. Whitehead and James, 2015; https://doi.org/10.1111/2041-210X.12383).

`list_DENMAT_real`: A list of 4 levels, one for each behaviour context. Each level contains one 34x34 matrix, containing the Simple-Ratio Index denominator to calculate the generalized affiliation indices (cf. Whitehead and James, 2015; https://doi.org/10.1111/2041-210X.12383).     
`list_RANDOM_DENMAT`: A list of 4 levels, one for each behaviour context. Each level contains 20000 random matrices (34x34), containing the Simple-Ratio Index denominator generated by random matrices to calculate random generalized affiliation indices (cf. Whitehead and James, 2015; https://doi.org/10.1111/2041-210X.12383).